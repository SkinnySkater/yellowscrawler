
#Update database and be sure to remove all the exytra ''
/*UPDATE restaurant
SET name = REPLACE(name, '\'', '');
UPDATE restaurant
SET phone = REPLACE(phone, '\'', '');
UPDATE restaurant
SET url = REPLACE(url, '\'', '');
UPDATE restaurant
SET street = REPLACE(street, '\'', '');
UPDATE restaurant
SET city = REPLACE(city, '\'', '');
UPDATE restaurant
SET town = REPLACE(town, '\'', '');
UPDATE restaurant
SET zip = REPLACE(zip, '\'', '');
UPDATE restaurant
SET country = REPLACE(country, '\'', '');
UPDATE restaurant
SET facebook = REPLACE(facebook, '\'', '');
UPDATE restaurant
SET regular_site = REPLACE(regular_site, '\'', '');
UPDATE restaurant
SET email = REPLACE(email, '\'', '');
UPDATE restaurant
SET name = REPLACE(name, '\'', '');

UPDATE info
SET cooking = REPLACE(cooking, '\'', '');
UPDATE info
SET gastro = REPLACE(gastro, '\'', '');
UPDATE info
SET open_time = REPLACE(open_time, '\'', '');
UPDATE info
SET grade = REPLACE(grade, '\'', '');
UPDATE info
SET subway = REPLACE(subway, '\'', '');
UPDATE info
SET average_price = REPLACE(average_price, '\'', '');*/

#Get number of restaurant saved
SELECT COUNT(*) AS NumberOfRestaurant FROM restaurant;

#number or real email found
SELECT COUNT(email) AS NumberOfEmail
FROM restaurant
WHERE email LIKE '%@%.%';

#Select number of email not found
SELECT COUNT(email) AS NumberOfEmailNotFound
FROM restaurant
WHERE email = 'None found';

#Number of Facebook link catch
SELECT COUNT(facebook) AS NumberOfFacebookLink
FROM restaurant
WHERE facebook LIKE 'https://www.facebook.com/%';

#Number of Web site
SELECT COUNT(regular_site) AS NumberOfWebSite
FROM restaurant
WHERE regular_site != 'None found';


#Number Of Best Restaurant
SELECT COUNT(grade) AS NumberOfBestRestaurant
FROM info
WHERE grade = '5';

#select The best Restaurant
SELECT restaurant.name AS RESTAURANT_NAME, info.grade AS Stars
FROM restaurant
INNER JOIN info
ON restaurant.id = info.id
WHERE info.grade = '5';

#No rate restaurant
SELECT restaurant.name AS RESTAURANT_NAME
FROM restaurant
INNER JOIN info
ON restaurant.id = info.id
WHERE info.grade = 'None';

#restaurant with no informations
SELECT restaurant.name AS RESTAURANT_NAME
FROM restaurant
WHERE restaurant.regular_site = 'None found' AND restaurant.facebook = 'None found';
