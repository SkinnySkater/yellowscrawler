SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `restaurant`;
DROP TABLE IF EXISTS `info`;

CREATE TABLE restaurant (
    id            int(11)      NOT NULL PRIMARY KEY, 
    name          varchar(64)  NOT NULL,
    phone         varchar(20)  NOT NULL,
    url           varchar(168) NOT NULL,
    street        varchar(128) NOT NULL,
    city          varchar(128) NOT NULL,
    town          varchar(128) NOT NULL,
    zip           varchar(128) NOT NULL,
    country       varchar(128) NOT NULL,
    facebook      varchar(168) NOT NULL,
    regular_site  varchar(168) NOT NULL,
    email         varchar(132)
    );

CREATE TABLE info (
    id            int           NOT NULL UNIQUE,
    cooktype      varchar(30)   NOT NULL,
    open_time     varchar(150)  NOT NULL,
    atmos		  varchar(30),
    price		  varchar(30),
    rate		  varchar(10),
    PRIMARY KEY (id),
    CONSTRAINT fk_restinfo FOREIGN KEY (id) REFERENCES restaurant(id)
    );

