import re
import urllib2
import urlparse
from xresto import Restaurant
from connection import storeResto
from user_agent import generate_user_agent, generate_navigator

def download(url, user_agent=generate_user_agent(platform=('mac', 'linux')), proxy=None, num_retries=2):
	"""Download function with support for proxies"""
	print 'Downloading:', url
	headers = {'User-agent': user_agent}
	request = urllib2.Request(url, headers=headers)
	opener = urllib2.build_opener()
	if proxy:
		proxy_params = {urlparse.urlparse(url).scheme: proxy}
		opener.add_handler(urllib2.ProxyHandler(proxy_params))
	try:
		html = opener.open(request).read()
	except urllib2.URLError as e:
		print 'Download error:', e.reason
		html = None
		if num_retries > 0:
			if hasattr(e, 'code') and 500 <= e.code < 600:
				# retry 5XX HTTP errors
				html = download(url, user_agent, proxy, num_retries-1)
	return html

def link_crawler(seed_url, page, end):
	crawl_queue = [seed_url]
	r = Restaurant()
	while crawl_queue:
		url = crawl_queue.pop()
		html = download(url)
		r.parse(html)
		"""print("Name : %s\nURL: %s\nStreet: %s\nCity: %s\ntown: %s\nZIP: %s\nCountry: %s  " % (r.name, r.fiche_url, r.street, r.city, r.town, r.zipc, r.country))
		print "facebook link : %s\nPhone number: %s\nRegular Link: %s" % (r.fb, r.phone, r.reg)
		storeResto(r.name, r.phone, r.fiche_url, r.street, r.city, r.town, r.zipc, r.country, r.fb, r.reg)"""
	print "name     : " + r.name
	print "street   : " + r.street
	print "city     : " + r.city
	print "area     : " + r.area
	print "zip code : " + r.zipc
	print "web site : " + r.reg
	print "facebook : " + r.fb
	print "cooking  : " + r.cooking
	print "avr price: " + r.price
	print "mood     : " + r.athmosphere
	print "rating   : " + r.rate
	print "phone    : " + r.phone
	print "open time: " + r.opentime

if __name__ == '__main__':
	url = 'http://www.pagesjaunes.fr/pros/53524267'
	pge = '/pros/'
	end = '.html'
	link_crawler(url, pge, end)