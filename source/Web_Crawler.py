from user_agent import generate_user_agent, generate_navigator
from SendEmail  import WarnMessage
from Restaurant import Restaurant
from IteratePageDriver import IteratePageDriver
from connection import storeResto, storeInfo
from datetime   import datetime

import re
import urlparse
import urllib2
import time
import time
import robotparser
import Queue

domain     = "http://www.pagesjaunes.fr"
pge        = "/pros/"
prgmMail   = "pokeskatego@gmail.com"
masterMail = "ovila.lugard@gmail.com"

noFb, noReg, = 0, 0

def link_crawler(seed_url, delay=5, max_depth=-1, max_urls=-1, headers=None, user_agent=None, proxy=None, num_retries=1):
    start_time = time.time()
    # the URL's that have been seen and at what depth
    seen = {seed_url: 0}
    # track how many URL's have been downloaded
    num_urls, Blocked, matching, PymysqlError = 0, 0, 0, 0
    rp = get_robots(seed_url)
    throttle = Throttle(delay)
    headers = headers or {}
    if user_agent:
        headers['User-agent'] = generate_user_agent(platform=('mac', 'linux'))
    #Prepare the selenium webdriver and load the first page
    webDriver = IteratePageDriver(seed_url)
    index = 1
    html  = download(seed_url, headers, proxy=proxy, num_retries=num_retries)
    total = get_total_page(html)
    print total
    while index <= total:
        print "Parsing the " + str(index) + "th page..."
        if index == 1:
            url = seed_url
        else:
            url = webDriver.get_Next_Page()
        # check url passes robots.txt restrictions
        if True:
            throttle.wait(url)
            html = download(url, headers, proxy=proxy, num_retries=num_retries)
            links = []
            # can still crawl further
            # filter
            links.extend(link for link in get_links(html) if check_url(link))
            print links
            for link in links:
                link = normalize(seed_url, link)
                print link
                # check whether already crawled this link
                if link not in seen:
                    seen[link] = num_urls + 1
                    # check link is within same domain
                    if same_domain(seed_url, link):
                        # Download each link
                        throttle.wait(link)
                        html = download(link, headers, proxy=proxy, num_retries=num_retries)
                        #Extract data here
                        matching += 1
                        print "parsing"
                        r = Restaurant()
                        r.parse(html)
                        try:
                            print "storing in database"
                            storeResto(matching, r.name, r.phone, link, r.street, r.city, r.town, r.zipc, r.country, r.fb, r.reg, r.email)
                            storeInfo(matching, r.cooking, r.opentime, r.athmosphere, r.price, r.rate)

                        finally:
                            print "Parsing : OK\t Database Updated : OK"
                        num_urls += 1
            # check whether have reached downloaded maximum
            num_urls += 1

        index += 1
    webDriver.TearDown()
    print "Good URL format found: " + str(num_urls)
    SendFeedback(Blocked, num_urls, matching, PymysqlError, (time.time() - start_time))
    endMsg()
    
def SendFeedback(Blocked, num_urls, matching, PymysqlError, times):
    subject = "Warning : Crawling Process End SUCCESSFULLY"
    bodyMsg = "Vas y eteins le pc x ) \n"
    bodyMsg += str(Blocked)  + "\tURL blocked.\n"
    bodyMsg += str(noReg)    + "\tRegular URLs not found.\n"
    bodyMsg += str(noFb)     + "\tFacebook URLs not found.\n"
    bodyMsg += str(num_urls) + "\tTotal URL crawled.\n"
    bodyMsg += str(matching)  + "\tURL added in DataBase.\n"
    bodyMsg += str(PymysqlError) + "\tTotal Error from PyMysql module.\n"
    bodyMsg += "Website crawled in " +  str(times)  + "seconds.\n"
    WarnMessage(bodyMsg, subject, prgmMail, masterMail, host='localhost')

class Throttle:
    """Throttle downloading by sleeping between requests to same domain"""
    def __init__(self, delay):
        # amount of delay between downloads for each domain
        self.delay = delay
        # timestamp of when a domain was last accessed
        self.domains = {}
        
    def wait(self, url):
        domain = urlparse.urlparse(url).netloc
        last_accessed = self.domains.get(domain)

        if self.delay > 0 and last_accessed is not None:
            sleep_secs = self.delay - (datetime.now() - last_accessed).seconds
            if sleep_secs > 0:
                time.sleep(sleep_secs)
        self.domains[domain] = datetime.now()

def check_url(url):
    if url.find(pge) >= 0:
        return True
    else:
        return False

def download(url, headers, proxy, num_retries, data=None):
    print 'Downloading:', url
    request = urllib2.Request(url, data, headers)
    opener = urllib2.build_opener()
    html = ""
    if proxy:
        proxy_params = {urlparse.urlparse(url).scheme: proxy}
        opener.add_handler(urllib2.ProxyHandler(proxy_params))
    try:
        response = opener.open(request)
        html = response.read()
        code = response.code
    finally:
        return html


def normalize(seed_url, link):
    """Normalize this URL by removing hash and adding domain"""
    link, _ = urlparse.urldefrag(link) # remove hash to avoid duplicates
    return urlparse.urljoin(seed_url, link)


def same_domain(url1, url2):
    """Return True if both URL's belong to same domain"""
    return urlparse.urlparse(url1).netloc == urlparse.urlparse(url2).netloc


def get_robots(url):
    """Initialize robots parser for this domain"""
    rp = robotparser.RobotFileParser()
    rp.set_url(urlparse.urljoin(url, '/robots.txt'))
    rp.read()
    return rp
        

def get_links(html):
    # a regular expression to extract all links from the webpage
    webpage_regex = re.compile('<a[^>]+href=["\'](.*?)["\']', re.IGNORECASE)
    # list of all links from the webpage
    return webpage_regex.findall(html)

def get_total_page(html):
    searchObj = re.compile('(?<=<strong>Page 1</strong> / )(?:(?!<).)*', re.S)
    res = searchObj.findall(html)
    print res
    return int(res[0]) if len(res) > 0 else -1

def endMsg():
    print "  (`\ .-') /`  ('-..-. .-').-. .-')              .-') _   "
    print "   `.( OO ),'_(  OO\  ( OO \  ( OO )            (  OO) )  "
    print ",--./  .--. (,------;-----.\;-----.\ .-'),-----./     '._ "
    print "|      |  |  |  .---| .-.  || .-.  |( OO'  .-.  |'--...__)"
    print "|  |   |  |, |  |   | '-' /_| '-' /_/   |  | |  '--.  .--'"
    print "|  |.'.|  |_(|  '--.| .-. `.| .-. `.\_) |  |\|  |  |  |   "
    print "|         |  |  .--'| |  \  | |  \  | \ |  | |  |  |  |   "
    print "|   ,'.   |  |  `---| '--'  | '--'  /  `'  '-'  '  |  |   "
    print "'--'   '--'  `------`------'`------'     `-----'   `--'   "

