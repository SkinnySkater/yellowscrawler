import urllib2
import re
import urlparse


def crawl_sitemap(url):
	# download the sitemap file
	sitemap = download(url)
	# extract the sitemap links
	links = re.findall('<loc>(.*?)</loc>', sitemap)
	# download each link
	for link in links:
		html = download(link)
		# scrape html here
		# ...

"""Crawl from the given seed URL following links matched by link_regex"""
def link_crawler(seed_url, link_regex, max_depth=2):
	max_depth = 2
	seen = {}
	crawl_queue = [seed_url]
	# keep track which URL's have seen before
	seen = set(crawl_queue)
	while crawl_queue:
		url = crawl_queue.pop()
		# check url passes robots.txt restrictions
		if rp.can_fetch(user_agent, url):
			html = download(url)
			depth = seen[url]
			if depth != max_depth:
				for link in get_links(html):
					if link not in seen:
						seen[link] = depth + 1
						crawl_queue.append(link)
					# check if link matches expected regex
					if re.match(link_regex, link):
						# form absolute link
						link = urlparse.urljoin(seed_url, link)
						# check if have already seen this link
						if link not in seen:
							seen.add(link)
							crawl_queue.append(link)
		else:
			print 'Blocked by robots.txt:', url


"""Return a list of links from html"""
def get_links(html):
	# a regular expression to extract all links from the webpage
	webpage_regex = re.compile('<a[^>]+href=["\'](.*?)["\']', re.IGNORECASE)
	# list of all links from the webpage
	return webpage_regex.findall(html)