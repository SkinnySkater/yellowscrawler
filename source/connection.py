import pymysql

databaseName = "yellowpage"

def storeResto(Id, name, phone, url, street, city, town, zipc, country, facebook, reg, email):
	try:
		#Connect to database
		conn = pymysql.connect(host='127.0.0.1', unix_socket='/var/run/mysqld/mysqld.sock',
		user='root', passwd='kimchi', db='yellowpage')

		cur = conn.cursor()
		cur.execute("USE " + databaseName)
		#support latine char -> encode ascii
		cur.execute("SET NAMES utf8mb4")

		cur.execute("INSERT INTO restaurant (id, name, phone, url, street, city, town, zip, country, facebook, regular_site, email) VALUES (\"%s\", \"%s\",\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\")"
		,(Id, name, phone, url, street, city, town, zipc, country, facebook, reg, email))
		#add in info table

		cur.connection.commit()
		#show the database state

	finally:
		#close conection
		cur.close()
		conn.close()


def storeInfo(Id, cooktype, open_time, atmos, price, rate):
	try:
		#Connect to database
		conn = pymysql.connect(host='127.0.0.1', unix_socket='/var/run/mysqld/mysqld.sock',
		user='root', passwd='kimchi', db='yellowpage')

		cur = conn.cursor()
		cur.execute("USE " + databaseName)
		#support latine char -> encode ascii
		cur.execute("SET NAMES utf8mb4")

		cur.execute("INSERT INTO info (id, cooktype, open_time, atmos, price, rate) VALUES (\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\")", (Id, cooktype, open_time, atmos, price, rate))

		cur.connection.commit()
		#show the database state

	finally:
		#close conection
		cur.close()
		conn.close()
