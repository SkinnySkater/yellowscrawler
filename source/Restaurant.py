# -*- coding: latin-1 -*- 
import re
import Web_Crawler
from user_agent import generate_user_agent, generate_navigator

class Restaurant:
	def __init__(self):
		self.name , self.phone , self.fiche_url, self.street, self.city, self.area, self.zipc, self.country, self.fb, self.reg = ("",)*10
		self.opentime, self.rate, self.cooking, self.athmosphere, self.price = ("",) * 5
		self.email = "None found" 

	def parse(self, html):
		self.country    = "France"
		self.town       = "Ile de France"
		self.name 		= get_name(html)
		self.city 		= get_city(html)
		self.zipc 		= get_zip(html)
		self.area 		= get_area(self.city)
		self.street 	= get_street(html)
		self.days 		= get_open_days(html)
		self.fb    		= get_Fblinks(html)
		self.reg   		= get_links(html)
		self.phone      = get_phone(html)
		self.opentime   = get_opentime(html)
		self.cooking    = get_cooking(html)
		self.rate       = get_rate(html)
		self.price      = get_price(html)
		self.athmosphere = get_atmos(html)

		if self.fb == 'None found':
			Web_Crawler.noFb += 1
		if self.reg == () or self.reg == 'None found':
			Web_Crawler.noReg += 1
		else:#Load the website and Search for the email
			throttle 			  = Web_Crawler.Throttle(2)
			headers				  = None
			headers 			  = headers or {}
			headers['User-agent'] = generate_user_agent(platform=('mac', 'linux'))
			throttle.wait(self.reg)
			html = Web_Crawler.download(self.reg, headers, proxy=None, num_retries=-1)
			self.email = get_email(html)

		print



def get_phone(html):
	searchObj = re.compile('(?<=<span class="coord-numero-mobile" itemprop="telephone" content=").(?:(?!").)*', re.S)
	res = searchObj.findall(html)
	return res[0] if len(res) > 0 else 'None found'

def get_Fblinks(html):
	searchObj = re.compile('(?<=<span class="value">www.facebook.com)(?:(?!<).)*', re.S)
	res = searchObj.findall(html)
	return "www.facebook.com" + res[0] if len(res) > 0 else 'None found'

def get_links(html):
	searchObj = re.compile('(?<=info::sites_et_reseaux_sociaux::lvs_payant&#039;,&#039;A&#039;\);","pos":"0"}\' title=")(?:(?! ).)*', re.S)
	res = searchObj.findall(html)
	#Get ay Facebook link
	for item in res:
		if item.find("facebook") != -1:
			self.fb = item
			#Remove fb link
			res = res.remove(item)
	return res[0] if len(res) > 0 and res[0].find("facebook") != 1 else 'None found'

def get_rate(html):
	searchObj = re.compile(r'(?<=ratingValue" content=")(?:(?!").)*' , re.IGNORECASE)
	res = searchObj.findall(html)
	return res[0] if len(res) > 0 else 'None found'

def get_opentime(html):
	searchObj = re.compile(r'(?<=<li itemprop="openingHours" content="...)(?:(?!").)*' , re.IGNORECASE)
	res = searchObj.findall(html)
	if len(res) > 13:
		#Setup the dictionary for rephrasing
		dico = {1: 'Lundi', 2: 'Mardi', 3: 'Mercredi', 4: 'Jeudi', 5: 'Vendredi', 6: 'Samedi', 7: 'Dimanche'}
		day   = 0
		flag  = False
		open1 = ""
		morning   = res[0].replace(':', 'h').replace('-', ' à ')
		afternoon = res[1].replace(':', 'h').replace('-', ' à ')
		for  id in xrange(2, len(res) - 2):
			if res[0] == res[1] and res[id] == res[id + 1]:
				day = day + 1
				id += 2
			else:
				flag = True			
				open1 = "Ouvert du Lundi au Samedi de " + morning + " et de " + afternoon
				open2 = "ouvert le Dimanche de " + res[12].replace(':', 'h').replace('-', ' à ') + " et de " + res[13].replace(':', 'h').replace('-', ' à ')
				return open1 + " " +open2
		if not flag:
			return "Ouvert tous les jours dU Lundi au Vendredi de "  + morning + " et de " + afternoon

	return 'None found'

def get_cooking(html):
	searchObj = re.compile(r'(?<=<li class="premiere-visibilite ">).(?:(?!<).)*' , re.S)
	res = searchObj.findall(html)
	if len(res) > 0:
		res[0] = res[0].replace(" ", "")
		res[0] = res[0].replace("\n", "")
		return res[0] 
	else:
		return 'None found'

def get_subway(html):
	searchObj = re.compile(r'(?<=restaurant-paris-par-metro/).(?:(?!").)*' , re.IGNORECASE)
	res = searchObj.findall(html)
	return res[0] if len(res) > 0 else 'None found'

def get_email(html):
	searchObj = re.compile('(?<=mailto:).(?:[A-Za-z0-9\._+contact]+@[A-Za-z]+\....)*', re.IGNORECASE)
	res = searchObj.findall(html)
	return res[0] if len(res) > 0 else 'None found'


def get_name(html):
	searchObj = re.compile('(?<=<meta name="description" content=")(?:(?! Paris).)*', re.I)
	res = searchObj.findall(html)
	return re.sub(r"&quot;", "\"", res[0]) if len(res) > 0 else 'None found'

def get_open_days(html):
	searchObj = re.compile('(?<=<li class="horaire-ouvert">.<span>)(?:(?!<).)*', re.S)
	res = searchObj.findall(html)
	return res if len(res) > 0 else 'None found'

def get_open_times(html):
	searchObj = re.compile('(?<=<li class="horaire-ouvert">.<span>)(?:(?!<).)*', re.S)
	res = searchObj.findall(html)
	return res if len(res) > 0 else 'None found'
	
def get_street(html):
	searchObj = re.compile('(?<=<span itemprop="streetAddress">.)(?:(?!<).)*', re.S)
	res = searchObj.findall(html)
	if len(res) > 0:
		end = re.sub(r" r ", ", rue ", res[0]) if len(res) > 0 else res[0]
		end = re.sub(r"&quot;", "\"", end) if len(res) > 0 else end
		end = re.sub(r"&#039;", "'", end) if len(res) > 0 else end
		return end
	else:
		return 'None found'

def get_city(html):
	searchObj = re.compile('(?<=<span itemprop="addressLocality" content=")(?:(?!").)*', re.I)
	res = searchObj.findall(html)
	return res[0] if len(res) > 0 else 'None found'

def get_area(city):
	area = ""
	if city == 'PARIS':
		area = "Ile de France"
	return area

def get_zip(html):
	searchObj = re.compile('(?<=<span itemprop="postalCode" content=")(?:(?!").)*', re.S)
	res = searchObj.findall(html)
	return res[0] if len(res) > 0 else 'None found'

def get_price(html):
	searchObj = re.compile(r'(?<=Prix moyen).(?:(?!euros).)*' , re.IGNORECASE)
	res = searchObj.findall(html)
	if len(res) > 0:
		res[0] = res[0].replace(" : ", "Entre ")
		res[0] = res[0].replace("-", " et ")
		return res[0]
	return 'None found'

def get_atmos(html):
	searchObj = re.compile('(?<=</span> Ambiance</h3>)(?:(?!</ul>).)*', re.S)
	res = searchObj.findall(html)
	if len(res) > 0:
		res[0] = res[0].replace(" ", "")
		res[0] = res[0].replace("\n", "")
		res[0] = re.sub('<.*?>', '/', res[0]) + '/'
		res[0] = res[0].replace("///", "")
		return res[0]
	return 'None found'