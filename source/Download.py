from user_agent import generate_user_agent, generate_navigator
import urllib2


#Default setup of max tries to 2 otherwise too suspicious
#Use user-agent library to generate fake user agent
#and appear less suspicious !
def Download_Link(url, user_agent=generate_user_agent(platform=('mac', 'linux')), max_try=2):
	print 'Downloading:', url
	print 'Using user-agent: ', user_agent
	headers = {'User-agent': user_agent}
	request = urllib2.Request(url, headers=headers)
	try:
		html = urllib2.urlopen(request).read()
	except urllib2.URLError as e:
		print 'Download error:', e.reason
		html = None
		if max_try > 0:
			if hasattr(e, 'code') and 500 <= e.code < 600:
			# retry 5XX HTTP errors
				return download(url, user_agent, max_try - 1)
	return html

