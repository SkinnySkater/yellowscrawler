from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class IteratePageDriver():

    def __init__(self, base_url):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(20)
        self.base_url = base_url
        self.driver.get(self.base_url)
 
    def get_Next_Page(self):
        driver = self.driver
        driver.find_element_by_id("pagination-next").click()
        return self.driver.current_url

    def TearDown(self):
    	self.driver.quit()